<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\produk;


class Home extends Controller
{

	public function index()
	{

		$produks = produk::all();		
		return view('home',['produks'=> $produks]);
		
	}

	public function form_tambah()
	{

		return view ('form_tambah');

	}

	public function simpan(Request $request)
	{
		 $validator = $this->validate($request,
		 	[
        	'barang' => 'required|min:3|max:255',
        	'stok' => 'required'
        	
    		]);	

		 $produk = new produk;

		 $produk->nama_barang	= $request->barang;
		 $produk->stok 			= $request->stok;

		 $produk->save();
		 $request->session()->flash('status', 'Barang Berhasil ditambah');

		return view ('form_tambah');

	}


		public function form_update(Request $request)
	{
		 
		 $id = $request->id;
		 $produks = produk::find($id);

		 

		return view('form_update',['produks'=> $produks]);

	}

	public function ganti(Request $request)
	{

		$id = $request->id;

		$produk = produk::find($id);

		$produk->nama_barang	= $request->barang;
		$produk->stok	= $request->stok;

		$produk->save();

		$request->session()->flash('status', 'Barang id '. $id .' berhasil diubah ');

		$produks = produk::all();		
		return view('home',['produks'=> $produks]);

	}

	public function hapus(Request $request)
	{
		 
		 $id = $request->id;
		 $produks = produk::find($id);
		 $produks->delete();
		 

		 $request->session()->flash('status', 'Barang id '. $id .' berhasil dihapus ');

		 $produks = produk::all();		
		 return view('home',['produks'=> $produks]);

	}

    
}


?>