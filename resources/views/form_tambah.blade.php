@extends('layout')


@section('title')
List Produk
@endsection

@section('menu')
Form Tambah
@endsection

@section('content')
 
 @if (count($errors) > 0)

 <ul>
  @foreach ($errors->all() as $error)
  <li style="color: red ">{{ $error }}</li>
  @endforeach
 </ul>

 @endif

@if (Session::has('status'))
{{ Session::get('status') }}
@endif
    {!! Form::open(['url' => 'save','method' => 'post', 'files'=>false]) !!}

            <div class="form-group">
                <label >Nama Barang</label>
                <input type="text" name="barang" class="form-control"  placeholder="Nama Barang" required>
            </div>

            <div class="form-group">
                <label >Stok</label>
                <input type="number" name="stok" class="form-control"  placeholder="Stok" required>
            </div>

            <button style="float: right;" type="Submit" class="btn btn-primary">Submit</button>




        {{ csrf_field() }}    
    {!! Form::close() !!}







@endsection