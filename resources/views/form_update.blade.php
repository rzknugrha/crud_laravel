@extends('layout')


@section('title')
List Produk
@endsection

@section('menu')
Form Tambah
@endsection

@section('content')
 
 @if (count($errors) > 0)

 <ul>
  @foreach ($errors->all() as $error)
  <li style="color: red ">{{ $error }}</li>
  @endforeach
 </ul>

 @endif

    {!! Form::open(['url' => 'update','method' => 'post', 'files'=>false]) !!}

            <div class="form-group">
                <label >Nama Barang</label>
                <input type="text" name="barang" class="form-control"  placeholder="Nama Barang"  value="{{ $produks->nama_barang }}" required>
            </div>

            <div class="form-group">
                <label >Stok</label>
                <input type="text" name="stok" class="form-control"  placeholder="Stok" value=" {{ $produks->stok }} " required>
            </div>

            <input type="hidden" name="id" class="form-control"  value="{{ $produks->id }}">
            <button style="float: right;" type="Submit" class="btn btn-primary">Submit</button>




        {{ csrf_field() }}    
    {!! Form::close() !!}







@endsection