@extends('layout')


@section('title')
List Produk
@endsection

@section('menu')
Menu Awal
@endsection




@section('content')

@if (Session::has('status'))
{{ Session::get('status') }}
@endif

 		<table class="table">
 		<a class="btn btn-outline-dark" role="button" href="form" style="float: right;"> Tambah +</a>
 		<tr>
 			<th>ID</th>
 			<th>Nama Barang</th>
 			<th>Stok</th>
 			<th>Aksi</th>
  		</tr>
  		
		@foreach ($produks as $produk)
		<tr>
            <td>{{ $produk->id  }}</td>
            <td>{{ $produk->nama_barang }}</td>
            <td>{{ $produk->stok }}</td>
            <td>
            <a  class="btn btn-primary" role='button' href="{{url('form_update/'.$produk->id) }}"> update </a> 
            <a class="btn btn-danger" role='button' href="{{url('delete/'.$produk->id) }}">delete</a> 
            </td>
        </tr>    
        @endforeach
        
        </table>	
@endsection