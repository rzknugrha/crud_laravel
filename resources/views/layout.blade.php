<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

		 <title>@yield('title')</title>
	 </head>
	<nav class="navbar navbar-light bg-light">
	  <span class="h1" class="navbar-brand mb-0">@yield('menu') </span>
	</nav>
	<body>
		<div class="container-fluid">
		      <div class="row">
		        	<nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar h-100" >
			          <ul class="nav nav-pills flex-column">
			            <li class="nav-item">
			              <a class="nav-link" href="{{url('home')}}">List Produk</a>
			            </li>
			            <li class="nav-item">
			              <a class="nav-link" href="{{url('form')}}">Tambah Produk</a>
			            </li>
			          </ul>
		        	</nav>

		        <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
		  
		          	@yield('content')
		       

		        </main>
		      </div>
		

		</div>


        <footer  class="fixed-bottom bg-secondary">
        	<center>aa</center>


        </footer>
	</body>
 </html>