<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Home@index');
Route::get('/home', 'Home@index');
Route::get('/form', 'Home@form_tambah');
Route::post('/save', 'Home@simpan');
Route::get('/form_update/{id}', 'Home@form_update');
Route::post('/update', 'Home@ganti');
Route::get('/delete/{id}', 'Home@hapus');
